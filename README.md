# Crossref Funder Registry

The Funder Registry provides a common taxonomy of international funding body names that funding data participants should use to normalize funder names and IDs for deposit with Crossref.

The list should be used to present authors with a pre-populated "Funder Name" option at the time of submission, and can also be used to match funder names extracted from papers.

The list is available to download as an RDF file, and is freely available under a CC0 license waiver.

## Latest Version

The latest version of the registry is `v1.43`.

Funder count: 32,248.

31,969 active funders, 279 defunct or replaced.


A .csv file of the most recent version of the Registry can [also be downloaded](https://doi.crossref.org/funderNames?mode=list) Note that there is no version history at this time for the .csv format.
